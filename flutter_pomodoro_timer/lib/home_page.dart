import 'package:flutter/material.dart';
import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:flutter_pomodoro_timer/countdown_timer.dart';
import 'package:flutter_pomodoro_timer/pause_page.dart';

import 'package:flutter_pomodoro_timer/utils/constants.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:ndialog/ndialog.dart';

import 'package:ringtone_player/ringtone_player.dart';

class HomePage extends StatefulWidget {
  final List<Icon> timesCompleted = [];

  HomePage() {
    // Initialize times completed dot icons
    for (var i = 0; i < 3; i++) {
      timesCompleted.add(
        Icon(
          Icons.brightness_1_rounded,
          color: Colors.blueGrey,
          size: 5.0,
        ),
      );
    }
  }

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final CountDownController _clockController = CountDownController();
  Icon _clockButton = kPlayClockButton; // Initial value
  bool _isClockStarted = false; // Conditional flag
  Color backColor = Colors.white;
  int timeWorkDuration = 1;
  String workStatus = 'work';

  // Change Clock button icon and controller
  void switchClockActionButton() {
    if (_clockButton == kPlayClockButton) {
      _clockButton = kPauseClockButton;
      if (!_isClockStarted) {
        // Processed on init
        _isClockStarted = true;
        _clockController.start();
        setState(() {
          backColor = Colors.orange;
        });
      } else {
        // Processed on play
        setState(() {
          backColor = Colors.orange;
        });
        _clockController.resume();
      }
    } else {
      // Processed on pause
      _clockButton = kPlayClockButton;
      _clockController.pause();
      setState(() {
        backColor = Colors.yellow;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // Half Screen Dimensions
    final double width = MediaQuery.of(context).size.width / 2;
    final double height = MediaQuery.of(context).size.height / 2;
    final double heightScreen = MediaQuery.of(context).size.height;

    CountDownTimer _countDownTimer = CountDownTimer(
      duration: kWorkDuration,
      fillColor: Colors.white,
      onComplete: () {
        FlutterRingtonePlayer.play(
          android: AndroidSounds.notification,
          ios: IosSounds.glass,
          looping: true, // Android only - API >= 28
          volume: 0.9, // Android only - API >= 28
          asAlarm: true, // Android only - all APIs
        );
        _isClockStarted = false;
        _clockButton = kPlayClockButton;
        backColor = Colors.white;
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text("Buen trabajo"),
            content: Text("Tiempo de Café"),
            actions: <Widget>[
              ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateColor.resolveWith((states) => Colors.blue),
                  ),
                  child: Text("Vamos al Break !!"),
                  onPressed: () {
                    setState(() {
                      FlutterRingtonePlayer.stop();
                      Navigator.pop(context);
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PausePage()),
                      );
                      /* switchClockActionButton(); */
                    });
                  }),
            ],
          ),
        );
      },
    );

    CircularCountDownTimer clock = CircularCountDownTimer(
      controller: _clockController,
      isReverseAnimation: true,
      ringColor: Color(0xff000000),
      height: height,
      width: width,
      autoStart: false,
      duration: _countDownTimer.duration * 60,
      isReverse: true,
      textStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 28),
      fillColor: _countDownTimer.fillColor,
      backgroundColor: Color(0xFFffffff),
      strokeCap: StrokeCap.round,
      onComplete: _countDownTimer.onComplete,
    );

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            height: heightScreen,
            color: backColor,
            child: Column(
              children: <Widget>[
                Center(
                  child: clock,
                ),
                Text(
                  kWorkLabel,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 10),
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            switchClockActionButton();
                          });
                        },
                        child: Container(
                          width: 100,
                          height: 50,
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: _clockButton,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 10),
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => PausePage()),
                            );
                          });
                        },
                        child: Container(
                          width: 100,
                          height: 50,
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Icon(Icons.emoji_food_beverage),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
