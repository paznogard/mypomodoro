import 'package:flutter/material.dart';
import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:flutter_pomodoro_timer/countdown_timer.dart';
import 'package:flutter_pomodoro_timer/utils/constants.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:ndialog/ndialog.dart';

import 'home_page.dart';

class PausePage extends StatefulWidget {
  final List<Icon> timesCompleted = [];

  PausePage() {
    // Initialize times completed dot icons
    for (var i = 0; i < 3; i++) {
      timesCompleted.add(
        Icon(
          Icons.brightness_1_rounded,
          color: Colors.blueGrey,
          size: 5.0,
        ),
      );
    }
  }

  @override
  _PausePageState createState() => _PausePageState();
}

class _PausePageState extends State<PausePage> {
  final CountDownController _clockController = CountDownController();
  Icon _clockButton = kPlayClockButton; // Initial value
  bool _isClockStarted = false; // Conditional flag
  Color backColor = Colors.green;

  // Change Clock button icon and controller
  void switchClockActionButton() {
    if (_clockButton == kPlayClockButton) {
      _clockButton = kPauseClockButton;
      if (!_isClockStarted) {
        // Processed on init
        _isClockStarted = true;
        _clockController.start();
        setState(() {
          backColor = Colors.green;
        });
      } else {
        // Processed on play
        _clockController.resume();
      }
    } else {
      // Processed on pause
      _clockButton = kPlayClockButton;
      _clockController.pause();
      setState(() {
        backColor = Colors.yellow;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // Half Screen Dimensions
    final double height = MediaQuery.of(context).size.height / 2;
    final double width = MediaQuery.of(context).size.width / 2;
    final double heightScreen = MediaQuery.of(context).size.height;
    int indexTimesCompleted = 0;

    CountDownTimer _countDownTimer = CountDownTimer(
      duration: kLongBreakDuration,
      fillColor: Colors.white,
      onComplete: () {
        FlutterRingtonePlayer.play(
          android: AndroidSounds.notification,
          ios: IosSounds.glass,
          looping: true, // Android only - API >= 28
          volume: 1.0, // Android only - API >= 28
          asAlarm: false, // Android only - all APIs
        );
        setState(() async {
          widget.timesCompleted[indexTimesCompleted] = Icon(
            Icons.brightness_1_rounded,
            color: Colors.white,
            size: 5.0,
          );
          indexTimesCompleted++;
          await NDialog(
            dialogStyle: DialogStyle(titleDivider: true),
            title: Text("Regresa"),
            content: Text("Regresemos a trabajar"),
            actions: <Widget>[
              ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateColor.resolveWith((states) => Colors.blue),
                  ),
                  child: Text("Listo!!"),
                  onPressed: () {
                    setState(() {
                      FlutterRingtonePlayer.stop();
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => HomePage()),
                      );
                      /* switchClockActionButton(); */
                    });
                  }),
            ],
          ).show(context);
        });
      },
    );

    CircularCountDownTimer clock = CircularCountDownTimer(
      controller: _clockController,
      isReverseAnimation: true,
      ringColor: Color(0xff000000),
      height: height,
      width: width,
      autoStart: true,
      duration: _countDownTimer.duration * 60,
      isReverse: true,
      textStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 28),
      fillColor: _countDownTimer.fillColor,
      backgroundColor: Color(0xFFffffff),
      strokeCap: StrokeCap.round,
      onComplete: _countDownTimer.onComplete,
    );

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            height: heightScreen,
            color: backColor,
            child: Column(
              children: <Widget>[
                Center(
                  child: clock,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text('BREAK !!',
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 28)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 10),
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => HomePage()),
                            );
                          });
                        },
                        child: Container(
                          width: 100,
                          height: 50,
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Icon(Icons.work),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
